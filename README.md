# Marvel ComicViewer

It's a simple viewer of Marvel comics.


### Installing

Clone the repository and into the folder you can install de modules with:


```
* npm install
```

# To run the project in dev enviroment you can run

```
ionic serve
```

Or if you prefer runing on a device run:

```
ionic cordova run android
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

To run the test you can run it with:

* npm test


## Deployment

To publish it you can follow ionic documentation for Android: https://beta.ionicframework.com/docs/publishing/play-store
OR
Ios:
https://beta.ionicframework.com/docs/publishing/app-store

## Built With

* [Angular 7](https://angular.io/) - The web framework used
* [Ionic 4](https://beta.ionicframework.com/docs/)


## Version

## Authors

* **Juan Camargo** juan.camargo@pragma.com.co

## License

This project is licensed under the MIT License